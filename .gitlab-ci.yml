stages:
  - start_notification
  - test
  - test_notification
  - clean-up
  - clean-up_notification
  - end_notification

.notification: &notif
  tags:
    - gke
  image:
    name: gcr.io/solutions-team-280017/alpine:3.18.2
  before_script:
    - apk add curl

.setup: &setup
  tags:
    - gke
  image:
    name: gcr.io/solutions-team-280017/alpine-tools:3.18.2
  before_script:
    - terraform version

# start notification on slack channel altered-carbon
start_notif_slack:
  <<: *notif
  only:
    - pipeline
  stage: start_notification
  script:
    - |
      curl -X POST -H 'Content-type: application/json' \
      --data "{\"text\":\"Running Cloud Credential Templates Test $(date +%m/%d/%y): $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID\"}" \
      $SLACK_CHANNEL_WEBHOOK

# test aws-vpc-site-service-account cloudformation template
test_aws_vpc_site_sa_template:
  <<: *setup
  stage: test
  only:
    - pipeline
  script:
    - cd ./.test/aws_vpc_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan
    - terraform apply -auto-approve
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# test aws-tgw-site-service-account cloudformation template
test_aws_tgw_site_sa_template:
  <<: *setup
  stage: test
  only:
    - pipeline
  script:
    - cd ./.test/aws_tgw_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan
    - terraform apply -auto-approve
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# test az-vnet-site-role template
test_az_vnet_site_role_template:
  <<: *setup
  stage: test
  only:
    - pipeline
  script:
    - cd ./.test/azure_vnet_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan
    - terraform apply -auto-approve
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# test gcp-vpc-site-role template
test_gcp_vpc_site_role_template:
  <<: *setup
  stage: test
  only:
    - pipeline
  script:
    - cd ./.test/gcp_vpc_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan
    - terraform apply -auto-approve
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# test status notification on failure
test_notif_slack:
  <<: *notif
  only:
    - pipeline
  stage: test_notification
  script:
    - |
      curl -X POST -H 'Content-type: application/json' \
      --data "{\"text\":\"Cloud Credential Templates Test Failed! :face_palm: $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID\"}" \
      $SLACK_CHANNEL_WEBHOOK
  when: on_failure

# clean-up aws-vpc-site-service-accont cloudformation template
clean_aws_vpc_site_sa_template:
  <<: *setup
  stage: clean-up
  only:
    - pipeline
  script:
    - cd ./.test/aws_vpc_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan -destroy
    - terraform destroy -auto-approve
  when: always
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# clean-up aws-tgw-site-service-accont cloudformation template
clean_aws_tgw_site_sa_template:
  <<: *setup
  stage: clean-up
  only:
    - pipeline
  script:
    - cd ./.test/aws_tgw_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan -destroy
    - terraform destroy -auto-approve
  when: always
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# clean-up az-vnet-site-role template
clean_az_vnet_site_role_template:
  <<: *setup
  stage: clean-up
  only:
    - pipeline
  script:
    - cd ./.test/azure_vnet_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan -destroy
    - terraform destroy -auto-approve
    - az login --service-principal -u "${AZURE_CLIENT_ID}" -p="${AZURE_CLIENT_SECRET}" --tenant "${AZURE_TENANT_ID}" --output none
    - |
      if [ $(az group list --query "[?name=='f5xc-cc-az-vnet-site']" | grep -c f5xc-cc-az-vnet-site) != 0 ]; then
        az group delete --yes --name f5xc-cc-az-vnet-site --force-deletion-types Microsoft.Compute/virtualMachineScaleSets --force-deletion-types Microsoft.Compute/virtualMachines
      fi
  when: always
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# clean-up gcp-vpc-site-role template
clean_gcp_vpc_site_role_template:
  <<: *setup
  stage: clean-up
  only:
    - pipeline
  script:
    - cd ./.test/gcp_vpc_site
    - aws s3 cp s3://${AWS_S3_BUCKET}/${VOLTERRA_TENANT}/machine.api-creds.p12 /
    - bash create-tfvars.sh
    - terraform init -backend-config=s3_backend -upgrade
    - terraform providers
    - terraform plan -destroy
    - terraform destroy -auto-approve
    - bash clean-up-gcp-resources.sh
  when: always
  retry:
    max: 1
    when:
      - script_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# clean-up status notification on failure
clean_notif_slack:
  <<: *notif
  only:
    - pipeline
  stage: clean-up_notification
  script:
    - |
      curl -X POST -H 'Content-type: application/json' \
      --data "{\"text\":\"Cloud Credential Templates Clean-up Failed! :face_palm: $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID\"}" \
      $SLACK_CHANNEL_WEBHOOK
  when: on_failure

# end notification on slack
end_notif_slack:
  <<: *notif
  only:
    - pipeline
  stage: end_notification
  script:
    - |
      curl -X POST -H 'Content-type: application/json' \
      --data "{\"text\":\"Cloud Credential Templates Test Scuccess! :party_parrot: $(date +%m/%d/%y): $CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID\"}" \
      $SLACK_CHANNEL_WEBHOOK