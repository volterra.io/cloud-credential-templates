# local variables
locals {
  azure_role_actions = jsondecode(file("${path.module}/../../azure/f5xc-azure-custom-role.json"))["Actions"]
  client_id          = azuread_application.app.application_id
  tenant_id          = var.tenant_id
  client_secret      = azuread_application_password.app_pass.value
}