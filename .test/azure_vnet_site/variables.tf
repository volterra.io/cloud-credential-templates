# variables

## azure access
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}


## volterra config
variable "name" {}
variable "azure_region" {}

# f5xc variables
variable "ssh_key" {
  type        = string
  default     = ""
  description = "ssh key for accessing google cloud instance"
}

variable "api_p12_file" {
  type        = string
  default     = ""
  description = "location of the f5xc user api p12 credentials"
}

variable "api_url" {
  type        = string
  default     = "https://playground.console.ves.volterra.io/api"
  description = "f5xc tenant api url"
}
