
## create volterra azure credentials
resource "volterra_cloud_credentials" "az_creds" {
  depends_on = [azuread_application_password.app_pass]
  name       = format("%s-creds", var.name)
  namespace  = "system"
  azure_client_secret {
    client_id       = local.client_id
    subscription_id = var.subscription_id
    tenant_id       = local.tenant_id
    client_secret {
      clear_secret_info {
        url = "string:///${base64encode(local.client_secret)}"
      }
    }
  }
}

## create volterra azure vnet site
resource "volterra_azure_vnet_site" "site" {
  name           = var.name
  namespace      = "system"
  description    = "f5xc azure vnet site for cloud-credentials"
  azure_region   = var.azure_region
  resource_group = var.name

  azure_cred {
    name      = volterra_cloud_credentials.az_creds.name
    namespace = "system"
  }

  vnet {
    new_vnet {
      name         = var.name
      primary_ipv4 = "192.168.0.0/22"
    }
  }
  disk_size    = 40
  machine_type = "Standard_D3_v2"

  ingress_egress_gw {
    azure_certified_hw = "azure-byol-multi-nic-voltmesh"
    az_nodes {
      azure_az  = 1
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.0.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.0.128/25"
        }
      }
    }
    az_nodes {
      azure_az  = 2
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.1.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.1.128/25"
        }
      }
    }
    az_nodes {
      azure_az  = 3
      disk_size = 40
      inside_subnet {
        subnet_param {
          ipv4 = "192.168.2.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.2.128/25"
        }
      }
    }

    performance_enhancement_mode {
      perf_mode_l7_enhanced = true
    }

    no_network_policy        = true
    no_forward_proxy         = true
    no_inside_static_routes  = true
    no_outside_static_routes = true
    no_global_network        = true
    no_dc_cluster_group      = true
    not_hub                  = true
  }
  ssh_key                 = var.ssh_key
  no_worker_nodes         = true
  logs_streaming_disabled = true

  lifecycle {
    ignore_changes = [labels, description]
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on = [
    volterra_azure_vnet_site.site,
    volterra_cloud_credentials.az_creds,
    azurerm_role_definition.role,
    azuread_application.app,
    azuread_service_principal.app,
    azurerm_role_assignment.app_role,
    azuread_application_password.app_pass
  ]
  site_name       = var.name
  site_kind       = "azure_vnet_site"
  action          = "apply"
  wait_for_action = true
}
