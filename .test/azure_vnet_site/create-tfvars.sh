#!/usr/bin/env sh

echo "{
    \"subscription_id\": \"${AZURE_SUBSCRIPTION_ID}\",
    \"client_id\": \"${AZURE_CLIENT_ID}\",
    \"client_secret\": \"${AZURE_CLIENT_SECRET}\",
    \"tenant_id\": \"${AZURE_TENANT_ID}\",
    \"api_p12_file\": \"/machine.api-creds.p12\",
    \"api_url\": \"${VOLTERRA_API_URL}\",
    \"name\": \"f5xc-cc-az-vnet-site\",
    \"azure_region\": \"${AZURE_REGION}\",
    \"ssh_key\": \"${SSH_PUBLIC_KEY}\"
}" > ./terraform.tfvars.json

jq . terraform.tfvars.json

cat <<EOF > ./s3_backend
bucket  = "${AWS_S3_BUCKET}"
key     = "${S3_BUCKET_FOLDER}/azure_vnet_site_role/terraform.tfstate"
region  = "${AWS_DEFAULT_REGION}"
encrypt = true
EOF

cat ./s3_backend