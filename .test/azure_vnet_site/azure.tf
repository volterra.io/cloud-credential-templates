# resources
## create role
resource "azurerm_role_definition" "role" {
  name        = var.name
  scope       = data.azurerm_subscription.ves_plm.id
  description = "F5XC Custom Role to create Azure VNET site"

  permissions {
    actions     = local.azure_role_actions
    not_actions = []
  }

  assignable_scopes = [
    data.azurerm_subscription.ves_plm.id
  ]
}

## create an application
resource "azuread_application" "app" {
  depends_on   = [azurerm_role_definition.role]
  display_name = var.name
}

## create a service principal
resource "azuread_service_principal" "app" {
  depends_on = [azuread_application.app]
  client_id  = azuread_application.app.client_id
}

## assign role to the service principal
resource "azurerm_role_assignment" "app_role" {
  depends_on         = [azurerm_role_definition.role, azuread_service_principal.app]
  scope              = data.azurerm_subscription.ves_plm.id
  role_definition_id = azurerm_role_definition.role.role_definition_resource_id
  principal_id       = azuread_service_principal.app.id
}

## generate passoword for the application
resource "azuread_application_password" "app_pass" {
  depends_on        = [azuread_application.app, azurerm_role_assignment.app_role]
  application_id    = azuread_application.app.id
  end_date_relative = "240h"
}

# # accept azure marketplace agreement
# resource "azurerm_marketplace_agreement" "f5xc_azure_ce" {
#   provider  = azurerm.custom
#   publisher = "volterraedgeservices"
#   offer     = "entcloud_voltmesh_voltstack_node"
#   plan      = "freeplan_entcloud_voltmesh_voltstack_node_multinic"
# }

# resource "null_resource" "accept_agreement" {
#   depends_on = [
#     azuread_service_principal.app,
#     azuread_application_password.app_pass
#   ]
#   provisioner "local-exec" {
#     command = <<-EOD
#       # login to azure using new credentials
#       az login --service-principal -u "${local.client_id}" -p="${local.client_secret}" --tenant "${local.tenant_id}"
#       # accept marketplace agreement
#       az vm image accept-terms --offer "entcloud_voltmesh_voltstack_node" --plan "freeplan_entcloud_voltmesh_voltstack_node_multinic" --publisher "volterraedgeservices"
#     EOD
#   }
# }