# providers
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}