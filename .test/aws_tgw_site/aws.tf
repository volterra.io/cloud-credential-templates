
# resources
resource "aws_cloudformation_stack" "stack" {
  name          = format("%s-stack", var.name)
  template_body = file("${path.module}/../../aws/aws-tgw-site-service-account.yaml")
  parameters = {
    "Password"   = var.password
    "PolicyName" = var.policy_name
  }
  capabilities = ["CAPABILITY_NAMED_IAM"]

  lifecycle {
    ignore_changes = [parameters]
  }
}

# Create spoke VPCs to be attached to the AWS TGW site
resource "aws_vpc" "aws_tgw_spoke_vpc" {
  count            = 2
  cidr_block       = lookup(var.aws_tgw_spoke_vpc_cidr, count.index)
  instance_tenancy = "default"

  tags = {
    Name = format("%s-%s", var.name, count.index)
  }
}

# Create VPC subnets in the spoke VPCs
resource "aws_subnet" "aws_tgw_spoke_vpc_subnet" {
  count      = 2
  vpc_id     = aws_vpc.aws_tgw_spoke_vpc[count.index].id
  cidr_block = cidrsubnet(lookup(var.aws_tgw_spoke_vpc_cidr, count.index), 8, 0)

  tags = {
    Name = format("%s-subnet-%s", var.name, count.index)
  }
}