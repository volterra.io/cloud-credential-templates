# variables
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}

variable "aws_tgw_spoke_vpc_cidr" {
  default = {
    0 = "10.10.0.0/16"
    1 = "10.20.0.0/16"
  }
}

variable "password" {}
variable "policy_name" {}
variable "name" {}
variable "ssh_key" {
  default = ""
}

variable "api_p12_file" {}
variable "api_url" {}