resource "volterra_cloud_credentials" "aws_creds" {
  name      = format("%s-creds", var.name)
  namespace = "system"
  aws_secret_key {
    access_key = local.aws_access_key
    secret_key {
      clear_secret_info {
        url = "string:///${base64encode(local.aws_secret_key)}"
      }
    }
  }
}

resource "volterra_aws_tgw_site" "site" {
  depends_on  = [aws_vpc.aws_tgw_spoke_vpc, aws_subnet.aws_tgw_spoke_vpc_subnet]
  name        = var.name
  namespace   = "system"
  description = "f5xc aws tgw site for cloud-credentials"

  aws_parameters {
    aws_certified_hw = "aws-byol-multi-nic-voltmesh"
    aws_region       = var.aws_region
    ssh_key          = var.ssh_key
    disk_size        = "40"
    no_worker_nodes  = true
    instance_type    = "t3.xlarge"

    new_vpc {
      name_tag     = format("%s-vpc", var.name)
      primary_ipv4 = "192.168.0.0/22"
    }

    new_tgw {
      system_generated = true
    }

    az_nodes {
      aws_az_name            = format("%sa", var.aws_region)
      disk_size              = "80"
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.0.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.0.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name            = format("%sb", var.aws_region)
      disk_size              = "80"
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.1.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.1.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name            = format("%sc", var.aws_region)
      disk_size              = "80"
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.2.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.2.128/25"
        }
      }
    }

    aws_cred {
      name      = volterra_cloud_credentials.aws_creds.name
      namespace = "system"
    }
  }

  vpc_attachments {
    dynamic "vpc_list" {
      for_each = toset(aws_vpc.aws_tgw_spoke_vpc)
      content {
        vpc_id = vpc_list.value.id
      }
    }
  }

  vn_config {
    no_inside_static_routes  = true
    no_outside_static_routes = true
    no_global_network        = true
    no_dc_cluster_group      = true
    sm_connection_public_ip  = true
  }

  tgw_security {
    no_forward_proxy                   = true
    east_west_service_policy_allow_all = true
    no_network_policy                  = true
  }

  direct_connect_enabled {
    standard_vifs = true
    auto_asn      = true
  }

  offline_survivability_mode {
    no_offline_survivability_mode = true
  }

  performance_enhancement_mode {
    perf_mode_l7_enhanced = true
  }

  default_blocked_services = true
  logs_streaming_disabled  = true

  lifecycle {
    ignore_changes = [labels, description]
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on = [
    volterra_aws_tgw_site.site,
    volterra_cloud_credentials.aws_creds,
    aws_subnet.aws_tgw_spoke_vpc_subnet,
    aws_vpc.aws_tgw_spoke_vpc,
    aws_cloudformation_stack.stack
  ]
  site_name       = var.name
  site_kind       = "aws_tgw_site"
  action          = "apply"
  wait_for_action = true
}
