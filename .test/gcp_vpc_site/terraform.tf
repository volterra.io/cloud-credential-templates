# terraform init
terraform {
  backend "s3" {
    region  = "us-east-1"
    encrypt = true
  }
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    random = {
      source = "hashicorp/random"
    }
    volterra = {
      source = "volterraedge/volterra"
    }
  }
}
