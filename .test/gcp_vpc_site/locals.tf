# local variables
locals {
  permissions                = yamldecode(file("${path.module}/../../gcp/f5xc_gcp_vpc_role.yaml"))["includedPermissions"]
  role_id                    = google_project_iam_custom_role.gcp_role.role_id
  role_binding               = "projects/${var.gcp_project_id}/roles/${local.role_id}"
  service_account_email      = google_service_account.service_account.email
  member                     = "serviceAccount:${local.service_account_email}"
  service_account_key_base64 = google_service_account_key.service_account_key.private_key
}
