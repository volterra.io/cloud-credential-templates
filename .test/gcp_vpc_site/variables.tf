# gcp variables
variable "gcp_credentials_base64" {
  type        = string
  default     = ""
  description = "base64 encoded google cloud service account credentials"
}

variable "gcp_region" {
  type        = string
  default     = "us-west1"
  description = "google cloud region"
}

variable "gcp_zone" {
  type        = string
  default     = "us-west1-c"
  description = "google cloud zone in the region"
}

variable "gcp_project_id" {
  type        = string
  default     = ""
  description = "google cloud project where the resources belong"
}

variable "gcp_sa_name" {
  type        = string
  default     = "cc-gcp-vpc-site"
  description = "google cloud service account name"
}

variable "name" {
  type        = string
  default     = "f5xc_cc_gcp_vpc_site"
  description = "google cloud resource name"
}

# f5xc variables
variable "f5xc_name" {
  type        = string
  default     = "f5xc-cc-gcp-vpc-site"
  description = "f5xc gcp vpc site name"
}

variable "ssh_key" {
  type        = string
  default     = ""
  description = "ssh key for accessing google cloud instance"
}

variable "api_p12_file" {
  type        = string
  default     = ""
  description = "location of the f5xc user api p12 credentials"
}

variable "api_url" {
  type        = string
  default     = "https://playground.console.ves.volterra.io/api"
  description = "f5xc tenant api url"
}
