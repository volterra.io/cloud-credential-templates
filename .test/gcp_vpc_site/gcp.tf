## create gcp custom role
resource "google_project_iam_custom_role" "gcp_role" {
  role_id     = format("%s_%s", var.name, random_string.name.result)
  title       = format("%s_%s", var.name, random_string.name.result)
  description = "GCP role to create F5XC GCP VPC site"
  permissions = local.permissions
}

## create gcp service account
resource "google_service_account" "service_account" {
  depends_on   = [google_project_iam_custom_role.gcp_role]
  account_id   = var.gcp_sa_name
  display_name = var.gcp_sa_name
  project      = var.gcp_project_id
}

## bind gcp service account with role in the project
resource "google_project_iam_binding" "binding" {
  depends_on = [google_service_account.service_account]
  project    = var.gcp_project_id
  role       = local.role_binding
  members    = [local.member]
}

## create gcp service account key
resource "google_service_account_key" "service_account_key" {
  depends_on         = [google_project_iam_binding.binding]
  service_account_id = google_service_account.service_account.name
}

