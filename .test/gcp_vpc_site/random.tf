## generate random string for gcp custom role
resource "random_string" "name" {
  length  = 2
  special = false
  lower   = true
}