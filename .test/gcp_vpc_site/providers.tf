# providers
provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  zone        = var.gcp_zone
  credentials = base64decode(var.gcp_credentials_base64)
}

provider "random" {}

provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}
