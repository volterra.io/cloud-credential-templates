#!/usr/bin/env sh

echo "{
    \"gcp_credentials_base64\": \"${GOOGLE_APPLICATION_CREDENTIALS_BASE64}\",
    \"gcp_region\": \"${GCP_REGION}\",
    \"gcp_zone\": \"${GCP_ZONE}\",
    \"gcp_project_id\": \"${GCP_PROJECT_ID}\",
    \"gcp_sa_name\": \"f5xc-cc-gcp-vpc-site\",
    \"f5xc_name\": \"f5xc-cc-gcp-vpc-site\",
    \"api_p12_file\": \"/machine.api-creds.p12\",
    \"api_url\": \"${VOLTERRA_API_URL}\",
    \"name\": \"f5xc_cc_gcp_vpc_site\",
    \"ssh_key\": \"${SSH_PUBLIC_KEY}\"
}" > ./terraform.tfvars.json

jq . terraform.tfvars.json

cat <<EOF > ./s3_backend
bucket  = "${AWS_S3_BUCKET}"
key     = "${S3_BUCKET_FOLDER}/gcp_vpc_site_role/terraform.tfstate"
region  = "${AWS_DEFAULT_REGION}"
encrypt = true
EOF

cat ./s3_backend