
## create f5xc gcp credentials
resource "volterra_cloud_credentials" "gcp_creds" {
  depends_on = [
    google_service_account_key.service_account_key,
    google_project_iam_binding.binding,
    google_service_account.service_account,
    google_project_iam_custom_role.gcp_role
  ]
  name        = format("%s-creds", var.f5xc_name)
  namespace   = "system"
  description = "f5xc azure vnet site for cloud-credentials"
  gcp_cred_file {
    credential_file {
      clear_secret_info {
        url = "string:///${local.service_account_key_base64}"
      }
    }
  }
}

## create f5xc gcp vpc site
resource "volterra_gcp_vpc_site" "site" {
  name        = var.f5xc_name
  namespace   = "system"
  description = "f5xc gcp vpc site for cloud-credentials"
  gcp_region  = var.gcp_region

  cloud_credentials {
    name      = volterra_cloud_credentials.gcp_creds.name
    namespace = "system"
  }

  instance_type           = "n1-standard-8"
  disk_size               = 40
  ssh_key                 = var.ssh_key
  logs_streaming_disabled = true
  block_all_services      = true

  ingress_egress_gw {
    gcp_certified_hw = "gcp-byol-multi-nic-voltmesh"
    node_number      = 3
    gcp_zone_names   = [format("%s-a", var.gcp_region), format("%s-b", var.gcp_region), format("%s-c", var.gcp_region)]
    inside_network {
      new_network_autogenerate {
        autogenerate = true
      }
    }
    inside_subnet {
      new_subnet {
        primary_ipv4 = "192.168.0.0/24"
        subnet_name  = format("%s-inside-subnet", var.f5xc_name)
      }
    }

    outside_network {
      new_network_autogenerate {
        autogenerate = true
      }
    }
    outside_subnet {
      new_subnet {
        primary_ipv4 = "192.168.1.0/24"
        subnet_name  = format("%s-outside-subnet", var.f5xc_name)
      }
    }

    no_network_policy        = true
    no_forward_proxy         = true
    no_global_network        = true
    no_inside_static_routes  = true
    no_outside_static_routes = true

    performance_enhancement_mode {
      perf_mode_l7_enhanced = true
    }
  }

  offline_survivability_mode {
    no_offline_survivability_mode = true
  }

  lifecycle {
    ignore_changes = [labels, description]
  }
}

## get status of volterra gcp vpc site
resource "volterra_tf_params_action" "apply_site" {
  depends_on = [
    volterra_gcp_vpc_site.site,
    google_service_account_key.service_account_key,
    google_project_iam_binding.binding,
    google_service_account.service_account,
    google_project_iam_custom_role.gcp_role
  ]
  site_name       = var.f5xc_name
  site_kind       = "gcp_vpc_site"
  action          = "apply"
  wait_for_action = true
}
