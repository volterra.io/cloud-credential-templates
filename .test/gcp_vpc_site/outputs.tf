# outputs
output "volterra_tf_params_action" {
  value = volterra_tf_params_action.apply_site
}

output "gcp_service_account_key_base64" {
  value = local.service_account_key_base64
  sensitive = true
}