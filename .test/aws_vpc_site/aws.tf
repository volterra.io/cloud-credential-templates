# resources
resource "aws_cloudformation_stack" "stack" {
  name          = format("%s-stack", var.name)
  template_body = file("${path.module}/../../aws/aws-vpc-site-service-account.yaml")
  parameters = {
    "Password"   = var.password
    "PolicyName" = var.policy_name
  }
  capabilities = ["CAPABILITY_NAMED_IAM"]

  lifecycle {
    ignore_changes = [parameters]
  }
}