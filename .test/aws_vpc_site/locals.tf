# local variables
locals {
  aws_access_key = aws_cloudformation_stack.stack.outputs.AccessKey
  aws_secret_key = aws_cloudformation_stack.stack.outputs.SecretKey
}