#!/usr/bin/env bash

echo "{
    \"aws_access_key\": \"${AWS_ACCESS_KEY_ID}\",
    \"aws_secret_key\": \"${AWS_SECRET_ACCESS_KEY}\",
    \"aws_region\": \"${AWS_REGION}\",
    \"password\": \"${PASSWORD}\",
    \"policy_name\": \"F5XCAWSVPCSAPolicy\",
    \"api_p12_file\": \"/machine.api-creds.p12\",
    \"api_url\": \"${VOLTERRA_API_URL}\",
    \"name\": \"f5xc-cc-aws-vpc-site\",
    \"ssh_key\": \"${SSH_PUBLIC_KEY}\"
}" > ./terraform.tfvars.json

jq . terraform.tfvars.json

cat <<EOF > ./s3_backend
bucket  = "${AWS_S3_BUCKET}"
key     = "${S3_BUCKET_FOLDER}/aws_vpc_site_sa/terraform.tfstate"
region  = "${AWS_DEFAULT_REGION}"
encrypt = true
EOF

cat ./s3_backend