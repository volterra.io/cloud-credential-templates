# outputs
output "volterra_tf_params_action" {
  value = volterra_tf_params_action.apply_site
}

output "aws_access_key" {
  value = local.aws_access_key
}

output "aws_secret_key" {
  value = local.aws_secret_key
  sensitive = true
}