# variables
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}
variable "password" {}
variable "policy_name" {}
variable "name" {}
variable "ssh_key" {
  default = ""
}
variable "api_p12_file" {}
variable "api_url" {}