# terraform init
terraform {
  backend "s3" {
    region  = "us-east-1"
    encrypt = true
  }
  required_providers {
    volterra = {
      source = "volterraedge/volterra"
    }
  }
}