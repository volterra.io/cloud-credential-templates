
resource "volterra_cloud_credentials" "aws_creds" {
  name      = format("%s-creds", var.name)
  namespace = data.volterra_namespace.system.name
  aws_secret_key {
    access_key = local.aws_access_key
    secret_key {
      clear_secret_info {
        url = "string:///${base64encode(local.aws_secret_key)}"
      }
    }
  }
}

resource "volterra_aws_vpc_site" "site" {
  name        = var.name
  namespace   = data.volterra_namespace.system.name
  description = "f5xc aws vpc site for cloud-credentials"
  aws_region  = var.aws_region

  aws_cred {
    name      = volterra_cloud_credentials.aws_creds.name
    namespace = "system"
  }

  vpc {
    new_vpc {
      name_tag     = format("%s-vpc", var.name)
      primary_ipv4 = "192.168.0.0/22"
    }
  }

  ingress_egress_gw {
    aws_certified_hw = "aws-byol-multi-nic-voltmesh"
    az_nodes {
      aws_az_name            = format("%sa", var.aws_region)
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.0.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.0.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name            = format("%sb", var.aws_region)
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.1.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.1.128/25"
        }
      }
    }
    az_nodes {
      aws_az_name            = format("%sc", var.aws_region)
      reserved_inside_subnet = true
      workload_subnet {
        subnet_param {
          ipv4 = "192.168.2.0/25"
        }
      }
      outside_subnet {
        subnet_param {
          ipv4 = "192.168.2.128/25"
        }
      }
    }
    no_network_policy        = true
    no_forward_proxy         = true
    no_inside_static_routes  = true
    no_outside_static_routes = true
    no_global_network        = true
    no_dc_cluster_group      = true
  }

  lifecycle {
    ignore_changes = [labels, description]
  }

  disk_size                = "40"
  instance_type            = "t3.xlarge"
  ssh_key                  = var.ssh_key
  no_worker_nodes          = true
  logs_streaming_disabled  = true
  default_blocked_services = true
  direct_connect_enabled {
    standard_vifs = true
    auto_asn      = true
  }
  offline_survivability_mode {
    no_offline_survivability_mode = true
  }
}

resource "volterra_tf_params_action" "apply_site" {
  depends_on = [
    volterra_aws_vpc_site.site,
    volterra_cloud_credentials.aws_creds,
    aws_cloudformation_stack.stack
  ]
  site_name       = var.name
  site_kind       = "aws_vpc_site"
  action          = "apply"
  wait_for_action = true
}
